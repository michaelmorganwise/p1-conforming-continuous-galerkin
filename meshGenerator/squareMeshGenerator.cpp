#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
using namespace std;

const double widthOfL = 2, heightOfL = 2, widthOfLTop = 2, heightOfLBase = 2;

struct Node{
	double x;
	double y;
};

bool equals(double x, double y){
	return fabs(x-y)<.00000000000001;
}

bool isBoundaryNode(Node node){
	return (equals(node.x,0) || equals(node.y,0) || equals(node.x,widthOfL) || equals(node.y,heightOfL) || 
		(equals(node.x,widthOfLTop) && node.y >= heightOfLBase) ||
		(equals(node.y,heightOfLBase) && node.x >= widthOfLTop));
}

int main(int argc, char** argv){
	if(argc < 3){
		cerr << "Invalid Arguments.\n" << endl;
		cerr << "Proper Usage:\n\t" << argv[0] << " <m> <n>\n\n";
		return 1;
	}
	double elementWidth = 1.0/atof(argv[1]);
	double elementHeight = 1.0/atof(argv[2]);  
	double epsX = elementWidth/2;
	double epsY = elementHeight/2;
	ofstream nodeFile, elementFile;

	nodeFile.open("L_nodes.txt");
	elementFile.open("L_elements.txt");

	vector<Node> nodes;

	for(double y = 0; y - heightOfL < epsY; y+=elementHeight){
		for(double x = 0; x - ((y - heightOfLBase < epsY)?widthOfL:widthOfLTop) < epsX; x+=elementWidth){
			nodes.push_back((Node){x,y});
		}
	}
	
	for(int i = 0; i < nodes.size(); i++)
		nodeFile << nodes[i].x << " " << nodes[i].y << ((isBoundaryNode(nodes[i]))?" B\n":"\n");

	int numRows = ceil(heightOfL/elementHeight)+1;
	int numCols = ceil(widthOfL/elementWidth)+1;

	int numBaseRows = ceil(heightOfLBase/elementHeight)+1;
	int numTopCols = ceil(widthOfLTop/elementWidth)+1;

	
	for(int i = 0; i < numRows-1; i++){
		if(i < numBaseRows-1){
			for(int j = i*(numCols); j < (i+1)*numCols-1; j++){
				elementFile << j << " " << j+1 << " " << j+numCols << endl;
				elementFile << (j+1)+numCols << " " << j+numCols << " " << j+1 << endl;
			}
		}
		else if(i == numBaseRows-1){
			for(int j = (numBaseRows-1)*numCols+(i+1-numBaseRows)*numTopCols; j < (numBaseRows-1)*numCols+(i+2-numBaseRows)*numTopCols-1; j++){
				elementFile << j << " " << j+1 << " " << j+numCols << endl;
				elementFile << (j+1)+numCols << " " << j+numCols << " " << j+1 << endl;
			}
		}
		else{
			for(int j = numBaseRows*numCols+(i-numBaseRows)*numTopCols; j < numBaseRows*numCols+(i+1-numBaseRows)*numTopCols-1; j++){
				elementFile << j << " " << j+1 << " " << j+numTopCols << endl;
				elementFile << (j+1)+numTopCols << " " << j+numTopCols << " " << j+1 << endl;
			}
		}
	}
	
	nodeFile.close();
	elementFile.close();

	return 0;
}
