./product/p1FEM "meshes/40_40_L/L_nodes.txt" "meshes/40_40_L/L_elements.txt"

************************************************

	 MATH/COSC 574 FINAL PROJECT

************************************************

1. Create Mesh

	* Parsed Nodes from Mesh File in 10.7188 ms.

	* Parsed Elements from Mesh File in 13.7866 ms.

2. Generate Linear Basis

	* Linear Basis Generated in 0.018833 ms.

3. Calculate Global Stiffness Matrix

	* Global Stiffness Matrix Assembled in 62.4531 ms.

	See brief description below.

CSparse Version 3.1.0, Jun 1, 2012.  Copyright (c) Timothy A. Davis, 2006-2012
4641-by-4641, nzmax: 31859 nnz: 31859, 1-norm: 8.00206
    col 0 : locations 0 to 2
      0 : 4.00308
      1 : -0.999486
      79 : -0.999486
    col 1 : locations 3 to 7
      1 : 4.00308
      0 : -0.999486
      2 : -0.999486
      79 : 0.000514042
      80 : -0.999486
    col 2 : locations 8 to 12
      2 : 4.00308
      1 : -0.999486
      3 : -0.999486
      80 : 0.000514042
      81 : -0.999486
    col 3 : locations 13 to 17
      3 : 4.00308
      2 : -0.999486
      4 : -0.999486
      81 : 0.000514042
      82 : -0.999486
    col 4 : locations 18 to 22
      4 : 4.00308
      3 : -0.999486
      5 : -0.999486
      82 : 0.000514042
  ...

4. Calculate Load Vector

	* Load Vector Assembled in 10.9174 ms.

5. Solve Sparse Linear System using Cholesky Factorization

	* Linear System Solved in 31.9948 ms.

	* L2 Error: 0.00037071

	* H1 Seminorm Error: 0.0503493

	* H1 Norm Error: 0.0503507

	* Data File "solution.dat" Printed to Input Mesh Directory.

Completed execution in 188.908 ms.

