CC=g++-5
CXSPARSE_ROOT = /Users/mmwise/SuiteSparse/CXSparse
LIBS = -L/usr/local/lib -lm -L${CXSPARSE_ROOT}/Lib -lcxsparse
INCS = -I/usr/local/include -L${CXSPARSE_ROOT}/Include -I./src
CFLAGS = -std=c++11 -O3 $(INCS)
EXECUTABLES = $(PRODUCT)/p1FEM
OBJDIR = obj
OBJECTS =   $(addprefix $(OBJDIR)/, p1FEM.o Quadrature2D.o Basis2D.o)
PRODUCT = product
SOURCE = src

all: $(EXECUTABLES) 

.SUFFIXES: .cpp .o
$(OBJDIR)/%.o: $(SOURCE)/%.cpp
ifneq ("$(wildcard $(OBJDIR))", "$(OBJDIR)")
	mkdir $(OBJDIR)
endif
	$(CC) $(CFLAGS) -c $< -o $@

$(PRODUCT)/p1FEM: $(OBJECTS)
ifneq ("$(wildcard $(OBJDIR))", "$(OBJDIR)")
	mkdir $(PRODUCT)
endif
	$(CC) $(CFLAGS) -o $(PRODUCT)/p1FEM $(OBJECTS) $(LIBS)

run-10:
	./$(PRODUCT)/p1FEM "meshes/10_10_L/L_nodes.txt" "meshes/10_10_L/L_elements.txt"

run-20:
	./$(PRODUCT)/p1FEM "meshes/20_20_L/L_nodes.txt" "meshes/20_20_L/L_elements.txt"

run-30:
	./$(PRODUCT)/p1FEM "meshes/30_30_L/L_nodes.txt" "meshes/30_30_L/L_elements.txt"

run-40:
	./$(PRODUCT)/p1FEM "meshes/40_40_L/L_nodes.txt" "meshes/40_40_L/L_elements.txt"

run-10S:
	./$(PRODUCT)/p1FEM "meshes/10_10_S/L_nodes.txt" "meshes/10_10_S/L_elements.txt"

run-20S:
	./$(PRODUCT)/p1FEM "meshes/20_20_S/L_nodes.txt" "meshes/20_20_S/L_elements.txt"

run-3S:
	./$(PRODUCT)/p1FEM "meshes/3_3_S/L_nodes.txt" "meshes/3_3_S/L_elements.txt"

run-40S:
	./$(PRODUCT)/p1FEM "meshes/40_40_S/L_nodes.txt" "meshes/40_40_S/L_elements.txt"

run-80:
	./$(PRODUCT)/p1FEM "meshes/80_80_L/L_nodes.txt" "meshes/80_80_L/L_elements.txt"

run-80S:
	./$(PRODUCT)/p1FEM "meshes/80_80_S/L_nodes.txt" "meshes/80_80_S/L_elements.txt"

run-160S:
	./$(PRODUCT)/p1FEM "meshes/160_160_S/L_nodes.txt" "meshes/160_160_S/L_elements.txt"

run-100:
	./$(PRODUCT)/p1FEM "meshes/100_100_L/L_nodes.txt" "meshes/100_100_L/L_elements.txt"

run-500:
	./$(PRODUCT)/p1FEM "meshes/500_500_L/L_nodes.txt" "meshes/500_500_L/L_elements.txt"

run-1000:
	./$(PRODUCT)/p1FEM "meshes/1000_1000_L/L_nodes.txt" "meshes/1000_1000_L/L_elements.txt"

run-1000S:
	./$(PRODUCT)/p1FEM "meshes/1000_1000_S/L_nodes.txt" "meshes/1000_1000_S/L_elements.txt"

run-100S:
	./$(PRODUCT)/p1FEM "meshes/100_100_S/L_nodes.txt" "meshes/100_100_S/L_elements.txt"

clean:
	/bin/rm -rf $(OBJDIR)/*.o $(EXECUTABLES) 

debug:
	ddd $(PRODUCT)/p1FEM
