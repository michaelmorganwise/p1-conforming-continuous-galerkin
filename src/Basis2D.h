#ifndef BASIS_2D
#define BASIS_2D

#include <vector>
#include "Quadrature2D.h"

struct Gradient2D {
	double x;
	double y;
};

class Basis2D {
public:
	Basis2D(int degreeOfBasis);
	const std::vector<std::vector<double>>& getBasisValuesOnMasterElement();
	const std::vector<std::vector<Gradient2D>>& getGradientValuesOnMasterElement();
	const std::vector<QuadraturePoint2D>& getQuadraturePoints2D();
	int getNumberOfDegreesOfFreedom();

private:
	const std::vector<QuadraturePoint2D>& quadraturePoints2D;
	std::vector< std::vector<double> > basisValuesAtMasterQuadraturePoints;
	std::vector< std::vector<Gradient2D> > gradientValuesAtMasterQuadraturePoints;
	std::vector<double> getBasisValuesAtPoint(double x, double y, int degree);
	std::vector<Gradient2D> getGradientValuesAtPoint(double x, double y, int degree);
	int numberOfDegreesOfFreedom;

};

#endif
