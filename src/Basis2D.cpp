#include <vector>
#include <exception>
#include "Basis2D.h"
#include "Quadrature2D.h"

Basis2D::Basis2D(int degreeOfBasis):
  quadraturePoints2D(Quadrature2D::getQuadraturePoints(8))
{
  QuadraturePoint2D point2D;
  double s;
  double t;
  double x;
  double y;
  numberOfDegreesOfFreedom = (degreeOfBasis+1)*(degreeOfBasis+2)/2;

  // Calculate basis values at master element quadrature points
  int size = quadraturePoints2D.size();
  for (int index = 0; index < size; index++) {
    point2D = quadraturePoints2D.at(index);
    basisValuesAtMasterQuadraturePoints.push_back(getBasisValuesAtPoint(
          point2D.x, point2D.y, degreeOfBasis));
    gradientValuesAtMasterQuadraturePoints.push_back(
        getGradientValuesAtPoint(point2D.x, point2D.y, degreeOfBasis));
  }
}

const std::vector<std::vector<double>>& Basis2D::getBasisValuesOnMasterElement(){
  return basisValuesAtMasterQuadraturePoints;
}

const std::vector< std::vector<Gradient2D>>& Basis2D::getGradientValuesOnMasterElement(){
  return gradientValuesAtMasterQuadraturePoints;
}

const std::vector<QuadraturePoint2D>& Basis2D::getQuadraturePoints2D(){
  return quadraturePoints2D;
}

std::vector<double> Basis2D::getBasisValuesAtPoint(double x, double y, int degree){
  std::vector<double> returnVector;

  // To-do: add higher degree basis functions later.
  switch (degree) {
    case 1:
      returnVector.push_back(1-x-y);
      returnVector.push_back(x);
      returnVector.push_back(y);
      break;
    default:
      //throw std::runtime_error("Basis functions of degree higher than 1 not yet supported.");
      break;
  }

  return returnVector;
}

std::vector<Gradient2D> Basis2D::getGradientValuesAtPoint(double x, double y, int degree){
  std::vector<Gradient2D> returnVector;
  Gradient2D grad;

  // To-do: add higher degree basis functions later.
  switch (degree) {
    case 1:
      grad.x = -1;
      grad.y = -1;
      returnVector.push_back(grad);
      grad.x = 1;
      grad.y = 0;
      returnVector.push_back(grad);
      grad.x = 0;
      grad.y = 1;
      returnVector.push_back(grad);
      break;
    default:
      //throw std::runtime_error("Basis functions of degree higher than 1 not yet supported.");
      break;
  }

  return returnVector;
}

int Basis2D::getNumberOfDegreesOfFreedom(){
  return numberOfDegreesOfFreedom;
}
