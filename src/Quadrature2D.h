#ifndef QUADRATURE_2D
#define QUADRATURE_2D

#include <vector>

struct QuadraturePoint2D {
	double x;
	double y;
	double weight;
};

class Quadrature2D {
public:
	static const std::vector<QuadraturePoint2D>& getQuadraturePoints(int degreeOfPolynomial);
private:
	static const std::vector<std::vector<QuadraturePoint2D> > quadraturePoints;
};

#endif
