/***************************************************
 
 Math/COSC 574 Final Exam Project
 Author: Michael Morgan Wise
 
 ***************************************************/

#include <iostream>
#include <chrono>
#include <cmath>
#include <libgen.h>
#include <sstream>
#include <fstream>
#include <vector>
#include <Quadrature2D.h>
#include <Basis2D.h>
using namespace std;

//extern "C"{
	#include <cs.h>
//}

struct Node;
class Element;

vector<Node*>		getNodesFromMeshFile(char* filename, int& NUMDOF);
vector<Element*>	getElementsFromMeshFile(char *filename, vector<Node*> nodes);
cs*		calculateStiffnessMatrix(vector<Element*> elements, Basis2D basis, int NUMDOF);
double*			calculateLoadVector(vector<Element*> elements, Basis2D basis, int NUMDOF);
double			computeL2Error(double *approx, vector<Element*> elements, Basis2D basis);
double			computeH1SeminormError(double *approx, vector<Element*> elements, Basis2D basis);
void			printDataFile(const char *filename, vector<Node*> nodes, double* rhs, int NUMDOF);
void			freeMesh(vector<Node*> nodes, vector<Element*> elements);

double f(double x, double y);
double u(double x, double y);
double u_x(double x, double y);
double u_y(double x, double y);

int main(int argc, char** argv){
	cout << "\n************************************************" << endl;
	cout << "\n\t MATH/COSC 574 FINAL PROJECT\n\n";
	cout << "************************************************\n\n";
	
	auto globalStart = chrono::steady_clock::now();
	
	Node* node; Element* element;
	int NUMDOF;

	cout << "1. Create Mesh\n\n";

	auto start = chrono::steady_clock::now();	
	vector<Node*> nodes = getNodesFromMeshFile(argv[1], NUMDOF);
	auto end = chrono::steady_clock::now();	
	auto diff = end - start;
	cout << "\a\t* Parsed Nodes from Mesh File in "
		 << chrono::duration <double, milli> (diff).count() << " ms.\n\n" ;

	start = chrono::steady_clock::now();	
	vector<Element*> elements = getElementsFromMeshFile(argv[2], nodes);
	end = chrono::steady_clock::now();
	diff = end - start;
	cout << "\a\t* Parsed Elements from Mesh File in "
		 << chrono::duration <double, milli> (diff).count() << " ms.\n\n" ;
	
	cout << "2. Generate Linear Basis\n\n";
	
	start = chrono::steady_clock::now();
	Basis2D basis(1);
	end = chrono::steady_clock::now();
	diff = end - start;

	cout << "\a\t* Linear Basis Generated in "
		 << chrono::duration <double, milli> (diff).count()
		 << " ms.\n\n" ;
	
	cout << "3. Calculate Global Stiffness Matrix\n\n";
	
	start = chrono::steady_clock::now();
	cs* stiffness = calculateStiffnessMatrix(elements, basis, NUMDOF);
	end = chrono::steady_clock::now();
	diff = end - start;	

	cout << "\a\t* Global Stiffness Matrix Assembled in "
		 << chrono::duration <double, milli> (diff).count()
		 << " ms.\n" << "\n\tSee brief description below.\n\n";
	
	cs_print(stiffness,true);
	
	cout << "\n4. Calculate Load Vector\n\n";
	
	start = chrono::steady_clock::now();
	double* rhs = calculateLoadVector(elements, basis, NUMDOF);
	end = chrono::steady_clock::now();
	diff = end - start;	

	cout << "\a\t* Load Vector Assembled in "
		 << chrono::duration <double, milli> (diff).count()
		 << " ms.";
	
	cout << "\n\n5. Solve Sparse Linear System using Cholesky Factorization\n\n";
	
	start = chrono::steady_clock::now();
	cs_cholsol(0, stiffness, rhs);
	end = chrono::steady_clock::now();
	diff = end - start;
	
	cout << "\a\t* Linear System Solved in "
		 << chrono::duration <double, milli> (diff).count()
		 << " ms.\n\n";
	
	double L2Error = computeL2Error(rhs, elements, basis);
	cout << "\t* L2 Error: " << L2Error
		 << "\n\n";
	
	double H1SemiError = computeH1SeminormError(rhs, elements, basis);
	cout << "\t* H1 Seminorm Error: "
		 << H1SemiError << "\n\n";
	
	cout << "\t* H1 Norm Error: "
		 << pow(L2Error*L2Error+H1SemiError*H1SemiError,0.5) << "\n\n";
	
	char *location = argv[1];
	printDataFile((string(dirname(location))+"/solution.dat").c_str(), nodes, rhs, NUMDOF);
	
	cout << "\t* Data File \"solution.dat\" Printed to Input Mesh Directory.";
	
	delete[] rhs;
	cs_spfree(stiffness);
	freeMesh(nodes, elements);

	auto globalEnd = chrono::steady_clock::now();
	diff = globalEnd - globalStart;
	cout << "\n\nCompleted execution in "
		 << chrono::duration <double, milli> (diff).count()
		 << " ms.\n\n";
	
	return 0;
}

struct Node{
	double x;
	double y;
	int matrixIndex;
};

class Element{
public:
	Element(Node* a, Node* b, Node* c){
		nodes[0] = a;
		nodes[1] = b;
		nodes[2] = c;
		area = calculateArea(a,b,c);
		calculateatrf(atrf,a,b,c);
	}
	Node* nodes[3];
	double area;
	double atrf[2][2];
private:
	double calculateArea(Node *a, Node *b, Node *c){
		return fabs(c->x*(a->y - b->y) - b->x*(a->y - c->y) + a->x*(b->y - c->y))/2;
	}
	void calculateatrf(double atrf[2][2], Node *a, Node *b, Node *c){
		atrf[0][0] = b->x-a->x;
		atrf[0][1] = c->x-a->x;
		atrf[1][0] = b->y-a->y;
		atrf[1][1] = c->y-a->y;
	}
};

double f(double x, double y){
	return pow(M_PI,2)*sin(M_PI*x)*sin(M_PI*y);
}

double u(double x, double y){
	return (1.0/3.0)*sin(M_PI*x)*sin(M_PI*y);
}

double u_x(double x, double y){
	return (M_PI/3.0)*cos(M_PI*x)*sin(M_PI*y);
}

double u_y(double x, double y){
	return (M_PI/3.0)*cos(M_PI*y)*sin(M_PI*x);
}

vector<Node*> getNodesFromMeshFile(char* filename, int& NUMDOF){
	std::ifstream file(filename);
	std::stringstream fileStream, lineStream;
	string line;
	Node* node;
	vector<Node*> nodes;
	int globalNodalPointCounter = 0;
	if(file){
		fileStream << file.rdbuf();
		file.close();
	}
	else{
		cerr << "Node file could not be opened for parsing.\n";
		exit(1);
	}
	while(getline(fileStream,line)){
		lineStream.clear();
		lineStream.str(line);
		node = new Node();
		lineStream >> node->x >> node->y;
		if(!lineStream.eof()){
			node->matrixIndex = -1;
		}
		else{
			node->matrixIndex = globalNodalPointCounter++;
		}
		nodes.push_back(node);
	}
	
	NUMDOF = globalNodalPointCounter;
	return nodes;
}

vector<Element*> getElementsFromMeshFile(char *filename, vector<Node*> nodes){
	std::ifstream file;
	std::stringstream fileStream, lineStream;
	string line;
	Element* element;
	vector<Element*> elements;
	int a,b,c;
	file.open(filename);
	if(file){
		fileStream.clear();
		fileStream << file.rdbuf();
		file.close();
	}
	else{
		cerr << "Element file could not be opened for parsing.\n";
		exit(1);
	}
	while(getline(fileStream,line)){
		lineStream.clear();
		lineStream.str(line);
		lineStream >> a >> b >> c;
		element = new Element(nodes[a],nodes[b],nodes[c]);
		elements.push_back(element);
	}
	return elements;
}

cs* calculateStiffnessMatrix(vector<Element*> elements, Basis2D basis, int NUMDOF){
	const std::vector<QuadraturePoint2D>& quadraturePoints = basis.getQuadraturePoints2D();
	const std::vector< std::vector<Gradient2D> >& gradValues = basis.getGradientValuesOnMasterElement();
	std::vector<std::vector<Gradient2D>> gradientValues;
	const std::vector< std::vector<double> >& basisValues = basis.getBasisValuesOnMasterElement();
	cs *triplet = cs_spalloc(NUMDOF, NUMDOF, NUMDOF*NUMDOF, 1, 1);
	Gradient2D gradient;
	Element* element;
	double weight;
	int rowIndex, columnIndex;
	double x, y, twiceArea, value;
	int localNDOF = basis.getNumberOfDegreesOfFreedom();
	QuadraturePoint2D point;
	int numberOfElements = elements.size();
	gradientValues = gradValues;
	for(int e = 0; e < elements.size(); ++e){
		element = elements[e];
		twiceArea = 2*element->area;
		for (size_t p = 0, numberOfQuadraturePoints = gradientValues.size(); p < numberOfQuadraturePoints; ++p) {
			for (size_t f = 0, numFunctions = gradientValues[p].size(); f < numFunctions; ++f) {
				gradient = gradValues[p][f];
				gradientValues[p][f].x = element->atrf[1][1]*gradient.x-element->atrf[1][0]*gradient.y;
				gradientValues[p][f].y = -element->atrf[0][1]*gradient.x+element->atrf[0][0]*gradient.y;
			}
		}
		
		for (size_t p = 0, numberOfQuadraturePoints = gradientValues.size(); p < numberOfQuadraturePoints; ++p) {
			weight = quadraturePoints[p].weight;
			for (int i = 0; i < localNDOF; ++i) {
				rowIndex = element->nodes[i]->matrixIndex;
				if(rowIndex != -1){
					for (int j = i; j < localNDOF; ++j) {
						columnIndex = element->nodes[j]->matrixIndex;
						if(columnIndex != -1){
							value = weight * ((gradientValues[p][i].x*gradientValues[p][j].x+
								   gradientValues[p][i].y*gradientValues[p][j].y)/twiceArea
								  +M_PI*M_PI*basisValues[p][i]*basisValues[p][j]*twiceArea);
							cs_entry(triplet, rowIndex, columnIndex, value);
							if(rowIndex != columnIndex)
								cs_entry(triplet, columnIndex, rowIndex, value);
						}
					}
				}
			}
		}
	}
	cs *stiffness = cs_compress(triplet);
	cs_dupl(stiffness);
	return stiffness;
}

double* calculateLoadVector(vector<Element*> elements, Basis2D basis, int NUMDOF){
	double *rhs = new double[NUMDOF]();
	const std::vector<QuadraturePoint2D>& quadraturePoints = basis.getQuadraturePoints2D();
	const std::vector< std::vector<double> >& basisValues = basis.getBasisValuesOnMasterElement();
	double factor;
	Element* element;
	int localNDOF = basis.getNumberOfDegreesOfFreedom();
	int rowIndex;
	double x, y, twiceArea;
	QuadraturePoint2D point;
	int numberOfElements = elements.size();
	int numQuadPoints = quadraturePoints.size();
	
	for(int e = 0; e < numberOfElements; ++e){
		element = elements[e];
		twiceArea = element->area*2;
		for(int p = 0; p < numQuadPoints; ++p){
			point = quadraturePoints[p];
			x = element->atrf[0][0]*point.x+element->atrf[0][1]*point.y+element->nodes[0]->x;
			y = element->atrf[1][0]*point.x+element->atrf[1][1]*point.y+element->nodes[0]->y;
			factor = point.weight*twiceArea*f(x,y);
			for(int i = 0; i < localNDOF; ++i){
				rowIndex = element->nodes[i]->matrixIndex;
				if(rowIndex != -1){
					rhs[rowIndex] += factor*basisValues[p][i];
				}
			}
		}
	}
	return rhs;
}

double computeL2Error(double *approx, vector<Element*> elements, Basis2D basis){
	Element* element;
	double x, y;
	QuadraturePoint2D point;
	const std::vector<QuadraturePoint2D>& quadraturePoints = basis.getQuadraturePoints2D();
	int localNDOF = basis.getNumberOfDegreesOfFreedom();
	int rowIndex;
	double weight, twiceArea;
	const std::vector< std::vector<double> >& basisValues = basis.getBasisValuesOnMasterElement();
	
	double totalError = 0, cellError = 0, uexact = 0, uapprox;
	for(int e = 0; e < elements.size(); ++e){
		element = elements[e];
		twiceArea = element->area*2;
		cellError = 0;
		for(int p = 0, numQuadPoints = quadraturePoints.size(); p < numQuadPoints; ++p){
			point = quadraturePoints[p];
			weight = point.weight;
			x = element->atrf[0][0]*point.x+element->atrf[0][1]*point.y+element->nodes[0]->x;
			y = element->atrf[1][0]*point.x+element->atrf[1][1]*point.y+element->nodes[0]->y;
			uexact = u(x,y);
			uapprox = 0;
			for(int i = 0; i < 3; ++i){
				rowIndex = element->nodes[i]->matrixIndex;
				if(rowIndex != -1){
					uapprox += approx[rowIndex]*basisValues[p][i];
				}
			}
			cellError += weight*(uexact-uapprox)*(uexact-uapprox);
		}
		totalError += twiceArea*cellError;
	}
	return pow(totalError,0.5);
}

double computeH1SeminormError(double *approx, vector<Element*> elements, Basis2D basis){
	Element* element;
	double x, y, u_xVal, u_yVal, uh_xVal, uh_yVal, totalError = 0, cellError;
	QuadraturePoint2D point;
	const std::vector<QuadraturePoint2D>& quadraturePoints = basis.getQuadraturePoints2D();
	int localNDOF = basis.getNumberOfDegreesOfFreedom();
	int rowIndex;
	double weight, jac;
	const std::vector< std::vector<Gradient2D> >& gradientValues = basis.getGradientValuesOnMasterElement();
	Gradient2D gradient;
	
	for(int e = 0; e < elements.size(); ++e){
		element = elements[e];
		jac = element->area*2;
		cellError = 0;
		for(int p = 0, numQuadPoints = quadraturePoints.size(); p < numQuadPoints; ++p){
			point = quadraturePoints[p];
			weight = point.weight;
			x = element->atrf[0][0]*point.x+element->atrf[0][1]*point.y+element->nodes[0]->x;
			y = element->atrf[1][0]*point.x+element->atrf[1][1]*point.y+element->nodes[0]->y;
			u_xVal = u_x(x,y); u_yVal = u_y(x,y);
			uh_xVal = 0; uh_yVal = 0;
			for(int i = 0; i < localNDOF; ++i){
				rowIndex = element->nodes[i]->matrixIndex;
				if(rowIndex != -1){
					gradient = gradientValues[p][i];
					uh_xVal += approx[rowIndex]*(element->atrf[1][1]*gradient.x
							-element->atrf[1][0]*gradient.y)/jac;
					uh_yVal += approx[rowIndex]*(-element->atrf[0][1]*gradient.x
							+element->atrf[0][0]*gradient.y)/jac;
				}
			}
			cellError += weight*((u_xVal-uh_xVal)*(u_xVal-uh_xVal)+(u_yVal-uh_yVal)*(u_yVal-uh_yVal));
		}
		totalError += jac*cellError;
	}
	return pow(totalError,0.5);
}

void printDataFile(const char *filename, vector<Node*> nodes, double* rhs, int NUMDOF){
	Node* node;
	Node **globalDOFs = new Node*[NUMDOF];
	for(int i = 0; i < nodes.size(); ++i){
		node = nodes[i];
		if(node->matrixIndex != -1){
			globalDOFs[node->matrixIndex] = node;
		}
	}
	ofstream dataFile(filename);
	for(int i = 0; i < nodes.size(); ++i){
		dataFile << nodes[i]->x << "\t" << nodes[i]->y << "\t";
		if(nodes[i]->matrixIndex != -1){
			dataFile << rhs[nodes[i]->matrixIndex];
		}
		else{
			dataFile << u(nodes[i]->x,nodes[i]->y);
		}
		dataFile << "\t" << u(nodes[i]->x,nodes[i]->y)
				 << endl;
	}
	dataFile.close();
}

void freeMesh(vector<Node*> nodes, vector<Element*> elements){
	for(int i = 0; i < elements.size(); ++i){
		delete elements[i];
	}
	for(int i = 0; i < nodes.size(); ++i){
		delete nodes[i];
	}
}
